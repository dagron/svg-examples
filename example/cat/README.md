# SVG фигура кота

## Использованные фигуры

  - < circle >
  - < g >
  - < line >
  - < use >
  - < polyline >
  - < path >
  - < text >

## Использованные атрибуты
  
  - style
  - stroke
  - fill
  - transform

## Описание работы

  [Создание SVG графики][1]

## Сайт источник
 
  - [Создание SVG графики][1]
 
[1]: http://www.xiper.net/learn/svg/svg-essentials/creating-an-svg-graphic.html
